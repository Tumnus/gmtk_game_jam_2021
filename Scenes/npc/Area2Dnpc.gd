extends Area2D

var PlayerSingleton_present = false
var connected_to_signal = false
export var correct_item = "skeleton"
export var any_item_dialogue = ""
export var wrong_item_dialogue = "Want item "
export var correct_item_dialogue = "Got item "
export var fulfilled_item_dialogue = "Fulfilled!"
var fulfilled = false
export var CatToy_dialogue = "..."
export var Glasses_dialogue = "..."
export var RollerBlades_dialogue = "..."
export var Diary_dialogue = "..."
export var SunHat_dialogue = "..."
export var LoveLetters_dialogue = "..."
export var MagicStaff_dialogue = "..."



func _ready():
	$Label.text = ""


func _on_Area2Dnpc_body_entered(body):
	if body.name == "PlayerSingleton":
		if not connected_to_signal:
			PlayerSingleton.connect("give_item_signal", self, "get_item")
			connected_to_signal = true
#		$Label.text = "collision detected"
		if not fulfilled:
			$Speech.start_text_bubble(any_item_dialogue)
		else:
			$Speech.start_text_bubble(fulfilled_item_dialogue)
		PlayerSingleton_present = true
#		if PlayerSingleton.has_object:
#			PlayerSingleton.get_node("Speech").start_text_bubble("Give?")


func _on_Area2Dnpc_body_exited(body):
	if body.name == "PlayerSingleton":
		if connected_to_signal:
			PlayerSingleton.disconnect("give_item_signal", self, "get_item")
			connected_to_signal = false

		PlayerSingleton_present = false
		PlayerSingleton.get_node("Label").set_text("")


func item_is_correct_item():
	var item_name = PlayerSingleton.item_object.get_name()
	print(self.get_name() + ": got " + item_name + ", correct_item = " + correct_item)
	return item_name == correct_item


func get_item():
	print(self.get_name() + ": received give_item_signal")
	if PlayerSingleton_present:
		print(self.get_name() + ": player present")
		if item_is_correct_item():
#			$Label.text = correct_item_dialogue
			$Speech.start_text_bubble(correct_item_dialogue)
			Events.emit_signal("received_item_signal", true, PlayerSingleton.item_object.get_name())
			overtake_item()
			self.fulfilled = true
		else:
			print(self.get_name() + ": got item: " + PlayerSingleton.item_object.get_name())
			match PlayerSingleton.item_object.get_name():
				"CatToy":
					$Speech.start_text_bubble(CatToy_dialogue)
				"Glasses":
					$Speech.start_text_bubble(Glasses_dialogue)
				"RollerBlades":
					$Speech.start_text_bubble(RollerBlades_dialogue)
				"Diary":
					$Speech.start_text_bubble(Diary_dialogue)
				"SunHat":
					$Speech.start_text_bubble(SunHat_dialogue)
				"LoveLetter":
					$Speech.start_text_bubble(LoveLetters_dialogue)
				"MagicStaff":
					$Speech.start_text_bubble(MagicStaff_dialogue)
				_:
					$Speech.start_text_bubble(wrong_item_dialogue)
				
#			$Label.text = wrong_item_dialogue + " (wanted " + correct_item + ")"
			#Events.emit_signal("received_item_signal", false)

func overtake_item():
	print("overtake item")
	PlayerSingleton.remove_child(PlayerSingleton.item_object)
	PlayerSingleton.has_object = false
	self.add_child(PlayerSingleton.item_object)
