extends Node2D

# TODO: make objects unique
#const object = preload("res://Scenes/flying_object/flying_object.tscn")
export var object = preload("res://Scenes/flying_object/flying_object.tscn")
var new_flying_object

func _ready():
	new_flying_object = object.instance()
	Events.connect("received_item_signal", self, "_on_receive")
	

func _on_Timer_timeout():
	#Events.emit_signal("spawner_timer_ended", object.instance().get_name())
	print(object.instance().get_name() + ": timer triggered")
	if not PlayerSingleton.has_object:
		print(object.instance().get_name() + ": send item")
		spawn_new_object()
#		$AudioStreamPlayer2D.play()
	
func spawn_new_object():
	# Spawn a new flying_object
	# var new_flying_object = object.instance()
	# At the spawner location
	if self.has_node(new_flying_object.get_name()):
		new_flying_object.position = Vector2.ZERO
	else:
		self.add_child(new_flying_object)
	new_flying_object.linear_velocity = Vector2.ZERO
	new_flying_object.apply_impulse_towards_player()

func _on_receive(accepted, item_name):
	print(object.instance().get_name() + ": got on receive signal")
	if (object.instance().get_name() == item_name):
		print(object.instance().get_name() + ": stopped timer")
		$Timer.stop()
