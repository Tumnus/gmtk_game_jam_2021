extends climbable_object


func _on_bottom_body_entered(body):
	if can_climb(body):
		add_node_path_points_to_player(true)

func _on_bottom_body_exited(body):
	if can_climb(body):
		reset_player(false)


func _on_top_body_entered(body):
	if can_climb(body):
		add_node_path_points_to_player(false)

func _on_top_body_exited(body):
	if can_climb(body):
		reset_player(true)
		
		

func add_node_path_points_to_player(up_not_down):
	# Call base method
	.add_node_path_points_to_player(up_not_down)
	
	PlayerSingleton.scaling_points = [1.0, 0.9, 0.8, 0.5, 0.0, 0.0, 0.0, 0.5, 0.8, 0.9, 1.0]


func reset_player(up_not_down):
	.reset_player(up_not_down)

	PlayerSingleton.scaling_points = []
