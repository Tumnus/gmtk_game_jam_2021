extends StaticBody2D

class_name climbable_object


func _on_bottom_body_entered(body):
	if can_climb(body):
		add_node_path_points_to_player(true)

func _on_bottom_body_exited(body):
	if can_climb(body):
		reset_player(false)


func _on_top_body_entered(body):
	if can_climb(body):
		add_node_path_points_to_player(false)

func _on_top_body_exited(body):
	if can_climb(body):
		reset_player(true)


func can_climb(body):
	return body.name == "PlayerSingleton" and PlayerSingleton.climb_direction == 0

func add_node_path_points_to_player(up_not_down):
	var points = calculate_global_path_coordinates()
	if up_not_down:
		PlayerSingleton.ladder_points_up = points
	else:
		points.invert()
		PlayerSingleton.ladder_points_down = points
#	PlayerSingleton.get_node("Label").text = "Climb!"
	PlayerSingleton.can_climb = true
	PlayerSingleton.current_ladder = self


func reset_player(up_not_down):
#	PlayerSingleton.get_node("Label").text = ""
	PlayerSingleton.ladder_points_up = []
	PlayerSingleton.ladder_points_down = []
	PlayerSingleton.can_climb = false
#	if up_not_down:
#		PlayerSingleton.ladder_points_up = []
#		if PlayerSingleton.ladder_points_down.size() == 0:
#			PlayerSingleton.can_climb = false
#	else:
#		PlayerSingleton.ladder_points_down = []
#		if PlayerSingleton.ladder_points_up.size() == 0:
#			PlayerSingleton.can_climb = false


func calculate_global_path_coordinates():
	# Apparently this must be done by hand??
	# https://github.com/godotengine/godot/issues/22025
	var curve = $climbing_path.curve
	var global_points = []
	for p in range(curve.get_point_count()):
		global_points.append(global_transform.xform(curve.get_point_position(p)))
	return global_points
