extends KinematicBody2D

class_name Player

# Basic X motion
var direction = Vector2.ZERO
var velocity = Vector2.ZERO
export var speed = 500


# Climbing
# potential to climb
var ladder_points_up = []
var ladder_points_down = []
var can_climb = false

# Actually climbing
export var climb_speed = 500
var climb_direction = 0
var current_ladder_points
var current_ladder
var next_ladder_point = 0
var target

# For shrinking door
var scaling_points = []

# Item control
var has_object = false
signal give_item_signal()
var can_give_item = false
var item_object
var received_items = 0
const items_for_victory = 7


func _ready():
	Events.connect("received_item_signal", self, "_on_receive")
	$Speech.start_text_bubble("Climb and give...")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if can_climb:
		climb_direction = handle_climbing_input()
	
	handle_item_input()
#	if can_give_item:
#		handle_item_input()
		
	if climb_direction == 0:
		var direction = handle_direction_input()
			
		# Apply
		velocity = speed * direction * delta
		move_and_collide(velocity)
	
		animate_walking()
		
	else:
		# Stop accepting more climbing input on first entry
		if can_climb:
			can_climb = false
			if climb_direction == 1:
				current_ladder_points = ladder_points_up
			elif climb_direction == -1:
				current_ladder_points = ladder_points_down
				
			target = current_ladder_points[next_ladder_point]
		# Note: the ladder should add its points to the player
		# Tutorial: https://kidscancode.org/godot_recipes/ai/path_follow/
		# Move to next point
		if position.distance_to(target) < 5:
			next_ladder_point = next_ladder_point + 1
#			print("Climbing to point: ")
#			print(next_ladder_point)
			if next_ladder_point >= current_ladder_points.size():
#				print("Finished climbing")
				finish_climb()
			else:
				target = current_ladder_points[next_ladder_point]
		
		move_on_ladder(delta, target)
		animate_walking()


func move_on_ladder(delta, target):
	# TODO: if speed is too large, this can miss the point...
	direction = (target - position).normalized()
	if next_ladder_point == 0:
		# Walking to entry point
		velocity = direction * speed * delta
	else:
		# Actual climbing
		velocity = direction * climb_speed * delta
		
	var collision = move_and_collide(velocity)
	
	if scaling_points.size() > 0:
		var new_scale = scaling_points[next_ladder_point]
		self.scale = Vector2(new_scale, new_scale)


func finish_climb():
	next_ladder_point = 0
	if climb_direction == 1:
		climb_direction = 0
		self.ladder_points_up = []
		# Get ready to climb the opposite way
		current_ladder._on_top_body_entered(self)
	elif climb_direction == -1:
		climb_direction = 0
		self.ladder_points_down = []
		current_ladder._on_bottom_body_entered(self)
	can_climb = true


func animate_walking():
	if velocity.length() > 0.05:
		$Sprite.play("walk")
	else:
		$Sprite.play("idle")
	
	if direction.x > 0.1:
		$Sprite.flip_h = false
	elif direction.x < -0.1:
		$Sprite.flip_h = true
	

func handle_direction_input():
	# This ability is common to all states
	direction.y = 0
	if Input.is_action_pressed("ui_left"):
		direction.x = -1
	elif Input.is_action_pressed("ui_right"):
		direction.x = 1
	else:
		direction.x = 0
		pass
	return direction


func handle_climbing_input():
	# Todo: only allow 'up' for going up
	if Input.is_action_pressed("ui_up") and ladder_points_up.size()>0:
		climb_direction = 1
	elif Input.is_action_pressed("ui_down") and ladder_points_down.size()>0:
		climb_direction = -1
	else:
		climb_direction = 0
	return climb_direction


func handle_item_input():
	if Input.is_action_just_pressed("ui_give_item"):
		if has_object:
			emit_signal("give_item_signal")


func check_victory():
	if received_items >= items_for_victory:
		get_tree().change_scene("res://Scenes/Victory.tscn")


func _on_receive(accepted, item_name):
	received_items += 1
	print("received_items = ", received_items)
	check_victory()
