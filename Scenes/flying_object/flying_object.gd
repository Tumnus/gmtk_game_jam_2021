extends RigidBody2D

class_name flying_object

var direction
export var speed = 1

var is_stuck = false
var rng = RandomNumberGenerator.new()

export var item_name = "skeleton"


func _ready():
	#Events.connect("received_item_signal", self, "_on_receive")
	#Events.connect("spawner_timer_ended", self, "_on_spawner_timer_ended")
	apply_impulse_towards_player()
	

func apply_impulse_towards_player():
	# As a rigid body, just apply force once
	direction = PlayerSingleton.position - self.global_position
	var impulse = speed * direction

	apply_central_impulse(impulse)


func _on_flying_object_body_entered(body):
	if body.name == "PlayerSingleton" and not is_stuck and not body.has_object:
		# Create a new object, and attach it to the PlayerSingleton
		# TODO: disconnect this signal on the duplicate
		var new_object = self.duplicate()
		new_object.speed = 0
		new_object.position = Vector2.ZERO
		new_object.is_stuck = true
		new_object.linear_velocity = Vector2.ZERO
		body.add_child(new_object)
		
		PlayerSingleton.has_object = true
		PlayerSingleton.item_object = new_object
		
		get_tree().queue_delete(self)


#func _on_receive(accepted):
#	if accepted:
#		print("ACCEPTED")
#		# Temporary: will be an animation
#		PlayerSingleton.has_object = false
#		#get_tree().queue_delete(self)
#	else:
#		print("NOT accepted")
#		# Temporary: just throw it away
##		is_stuck = false
##		PlayerSingleton.has_object = false
##		var vector = Vector2((rng.randfn()-0.5)*100,(rng.randfn()-0.5)*100)
##		apply_central_impulse(vector)


#func _on_spawner_timer_ended(object_name):
#	# Delete if you didn't catch it
#	if not is_stuck and object_name == self.get_name():
#		get_tree().queue_delete(self)
#	else:
#		Events.disconnect("spawner_timer_ended", self, "_on_spawner_timer_ended")
